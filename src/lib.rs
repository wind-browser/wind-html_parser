use wind_dom::Node;

pub fn parse(html_document: &[u8]) -> Node {
	let mut document_root = Node::new("document");

	parse_rec(html_document, &mut document_root);

	document_root
}

/* Parse the html document recursively */
fn parse_rec(document: &[u8], parent_node: &mut Node) {
	let mut ptr = 0;

	// Skip over whitespace
	while document[ptr].is_ascii_whitespace() {
		ptr += 1;
	}

	// Logic if text node
	if document[ptr] != ('<' as u8) {
		let next_tag = find_next('<', document).unwrap(); // TODO not save
		let text_node = Node::new("#text");

		let contents = &document[0..next_tag];

		parse_rec(&document[next_tag..document.len()], parent_node);
		unimplemented!("Parsing #text node at {}.", ptr);
	}
	// Logic if new node
	else {
		// Find closing bracket
		let closing_bracket = find_next('>', document)
			.expect(format!("Bracket opened at {} not closed.", ptr).as_str()); // TODO: Fail gracefully

		// Extract text in between
		let _contents = &document[(ptr + 1)..closing_bracket];

		unimplemented!("Parsing tag at {}.", ptr);
	}
}

/* Find the next occurrence of a char `to_find` in an u8 array `document`.
The `starting_at` parameter indicates where to start in the array.
Returns the index, or if no occurence was found, returns None. */
fn find_next(to_find: char, document: &[u8]) -> Option<usize> {
	let mut ptr = 0;

	loop {
		// Check if out of bounds of document
		if document.len() >= ptr {
			return None;
		}

		if document[ptr] == (to_find as u8) {
			return Some(ptr);
		}

		ptr += 1;
	}
}
